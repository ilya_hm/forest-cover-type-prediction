# Forest Cover Type 
The idea of this notebook is to use the forest cover type dataset to find the predominant kind of tree cover using Deep learning.

![Drag Racing](photos/cover.jpg)

----

### Table Of Contents:
- [Description](#description)<br>
    - [About the project](#about-the-project)<br>
    - [feature extraction](#feature-extraction)<br>
- [Data](#data)<br>
    - [Files](#files)<br>
    - [Dataset file](#dataset)<br>

 - [Requirements](#requirements)<br>

----

### Description

#### About the project
This study includes 4 Wilderness Areas located in the Roosevelt National Forest of Northern Colorado. These area represent forests with minimal human-caused disturbances, so that existing forest cover types are more a result of ecological process rather than forest management practices.

Each observation is 30m x 30m forest cover type determined from US Forest Service (USFS) Region 2 Resource Information System (RIS) data. Independent variables were derived from the data originally obtained from US Geological Survey (USGS) and USFS data.

#### Feature extraction
two different features set were used:
- Extra Tree Classifier method (Feature selection) 
This method select the best features set based on theire importance.
- Feature engineering (Feature extraction)
Create new features using the combination between the real variables.

----

### Data

#### Files

- `model1.ipynb`: This notebook contain the first implementation with Extratree classifier.
- `model2.ipynb`: This notebook contain the second implementation with combination between the real variables.

#### Dataset 
 You can find the dataset here: https://www.kaggle.com/uciml/forest-cover-type-dataset

----

### Requirements

- [Python 3.6.6](https://www.python.org/downloads/release/python-366/)      
- [NumPy](http://www.numpy.org/)                                            
- [Pandas](http://pandas.pydata.org)                                        
- [matplotlib](http://matplotlib.org/)   
- [scikit-learn](http://scikit-learn.org/stable/)                           

----